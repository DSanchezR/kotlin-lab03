package com.dsanchez.kotlin_lab03

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRegistrar.setOnClickListener {

            val nombreMascota = edtNombreMascota.text.toString()
            val edadMascota = edtEdadMascota.text.toString()

            if (nombreMascota.isEmpty()) {
                this.mensaje("Debe de ingresar el nombre de la mascota")
                return@setOnClickListener
            }

            if (edadMascota.isEmpty()) {
                this.mensaje("Debe de ingresar el nombre de la mascota")
                return@setOnClickListener
            }

            var idRecursoTipoMascota: Int = 0

            if (rbPerro.isChecked) {
                idRecursoTipoMascota = R.drawable.avatar_dog_120_x120
            } else if (rbGato.isChecked) {
                idRecursoTipoMascota = R.drawable.avatar_cat_120_x120
            } else if (rbConejo.isChecked) {
                idRecursoTipoMascota = R.drawable.avatar_rabbit_120_x120
            }
            else{

                this.mensaje("Debe de seleccionar un tipo de mascota")
                return@setOnClickListener
            }

            val vacunaCoronavirus = chkCoronavirusMain.isChecked
            val vacunaHepatitis = chkHepatitisMain.isChecked
            val vacunaLeucemia = chkLeucemiaMain.isChecked
            val vacunaParvovirus = chkParvovirusMain.isChecked
            val vacunaRabia = chkRabiaMain.isChecked

            var bundle = Bundle().apply {
                putString("key_nombre_mascota", nombreMascota)
                putString("key_edad_mascota", edadMascota)
                putInt("key_idRecursoTipoMascota", idRecursoTipoMascota)
                putBoolean("key_vacuna_coronavirus", vacunaCoronavirus)
                putBoolean("key_vacuna_hepatitis", vacunaHepatitis)
                putBoolean("key_vacuna_leucemia", vacunaLeucemia)
                putBoolean("key_vacuna_parvovirus", vacunaParvovirus)
                putBoolean("key_vacuna_rabia", vacunaRabia)
            }

            val intent = Intent(this, InfoMascotaActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)

        }

    }


    fun mensaje(mensaje: String) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show()
    }


}