package com.dsanchez.kotlin_lab03

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_info_mascota.*

class InfoMascotaActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info_mascota)

        val bundle : Bundle? = intent.extras

        bundle?.let { bundleLibreDeNull ->

            val nombre =  bundleLibreDeNull.getString("key_nombre_mascota","Desconocido")
            val edad = bundleLibreDeNull.getString("key_edad_mascota","0")
            val idTipoMascota = bundleLibreDeNull.getInt("key_idRecursoTipoMascota",0)

            val isCheckedVacunaCoronavirus = bundleLibreDeNull.getBoolean("key_vacuna_coronavirus",false)
            val isCheckedVacunaHepatitis = bundleLibreDeNull.getBoolean("key_vacuna_hepatitis",false)
            val isCheckedVacuna_leucemia = bundleLibreDeNull.getBoolean("key_vacuna_leucemia",false)
            val isCheckedVacuna_parvovirus = bundleLibreDeNull.getBoolean("key_vacuna_parvovirus",false)
            val isCheckedVacuna_rabia = bundleLibreDeNull.getBoolean("key_vacuna_rabia",false)

            imgMascota.setImageResource(idTipoMascota)
            tvNombreMascota.text = nombre
            tvEdadMascota.text = edad + if (edad.toInt() == 1) " año" else " años"

            chkCoronavirusInfo.isChecked = isCheckedVacunaCoronavirus
            chkHepatitisInfo.isChecked = isCheckedVacunaHepatitis
            chkLeucemiaInfo.isChecked = isCheckedVacuna_leucemia
            chkParvovirusInfo.isChecked = isCheckedVacuna_parvovirus
            chkRabiaInfo.isChecked = isCheckedVacuna_rabia

        }

    }
}